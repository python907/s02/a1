# To determine a year if it is a Leap Year:
year = int(input("Please enter the year\n"))
leap_year = 4

if year % leap_year == 0:
	print("This is a leap year")
else:
	print("This is not a leap year")



# Make rows and columns:

# for x in range(3):
# 	print(f"*")
# for y in range(4):
# 	print(f"*")

rows = int(input("Please enter number of rows\n"))
columns = int(input("Please enter number of columns\n"))

x = 1
y = 1
table = ""
while x <= rows:
	while y <= columns:
		table += "*"
		y += 1
	print(table)
	x += 1